/**********************************************************************
  Copyright(c) 2011-2013 Intel Corporation All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions 
  are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
    * Neither the name of Intel Corporation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>  // for memset, memcmp
#include <unistd.h>
#include "erasure_code.h"
#include "test.h"

//#define CACHED_TEST
#ifdef CACHED_TEST
// Cached test, loop many times over small dataset
# define TEST_SOURCES 250
# define TEST_LEN(m)  ((256*1024 / m) & ~(64-1))
# define TEST_LOOPS(m)   (4000*m)
# define TEST_TYPE_STR "_warm"
#else
# ifndef TEST_CUSTOM
// Uncached test.  Pull from large mem base.
#  define TEST_SOURCES 250
#  define GT_L3_CACHE  32*1024*1024  /* some number > last level cache */
#  define TEST_LEN(m)  ((GT_L3_CACHE / m) & ~(64-1))
#  define TEST_LOOPS(m)   (10*m)
#  define TEST_TYPE_STR "_cold"
# else
#  define TEST_TYPE_STR "_cus"
#  ifndef TEST_LOOPS
#    define TEST_LOOPS(m) 1000
#  endif
# endif
#endif

#define MMAX TEST_SOURCES
#define KMAX TEST_SOURCES

typedef unsigned char u8;


void dump(unsigned char* buf, int len)
{
	int i;
	for (i=0; i<len; ) {
		printf(" %2x", 0xff & buf[i++]);
		if (i % 32 == 0)
			printf("\n");
	}
	printf("\n");
}

void dump_matrix(unsigned char **s, int k, int m)
{
	int i, j;
	for (i=0; i<k; i++) {
		for (j=0; j<m; j++){
			printf(" %2x", s[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void dump_u8xu8(unsigned char *s, int k, int m)
{
	int i, j;
	for (i=0; i<k; i++) {
		for (j=0; j<m; j++){
			printf(" %2x", 0xff & s[j+(i*m)]);
		}
		printf("\n");
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	int i, j, rtest, m, k, nerrs, r, err;
        int stripe_size, loop_multiplier;
	void *buf;
        u8 printed_errors = 0;
	u8 *temp_buffs[TEST_SOURCES], *buffs[TEST_SOURCES];
	u8 a[MMAX*KMAX], b[MMAX*KMAX], c[MMAX*KMAX], d[MMAX*KMAX];
	u8 g_tbls[KMAX*TEST_SOURCES*32], src_in_err[TEST_SOURCES];
	u8 src_err_list[TEST_SOURCES], *recov[TEST_SOURCES];
	struct perf start, stop;
        char ch;

	m = 32;
	k = 28;
        stripe_size = TEST_LEN(m);
        while((ch = getopt(argc, argv, "m:k:s:h")) != -1) {
            switch(ch) {
                case 'm':
                    m = atoi(optarg);
                    break;
                case 'k':
                    k = atoi(optarg);
                    break;
                case 's':
                    stripe_size = atoi(optarg);
                    break;
                default:
                    fprintf(stderr, "unknown option %c\n", ch);
                    exit(1);
            }
        }
        if (stripe_size > TEST_LEN(m)) {
            fprintf(stderr, "stripe_size too large: %d > %d. reduce\n", stripe_size, TEST_LEN(m));
            stripe_size = TEST_LEN(m);
        }
	printf("erasure_code_sse_perf: m=%d x test_len=%d, k=%d stripe_size=%d\n",
			m, (TEST_LEN(m)), k, stripe_size);


	// Allocate the arrays
	for(i=0; i<TEST_SOURCES; i++){
		if (posix_memalign(&buf, 64, TEST_LEN(m))) {
			printf("alloc error: Fail");
			return -1;
		}
		buffs[i] = buf;
	}

	for (i=0; i<TEST_SOURCES; i++){
		if (posix_memalign(&buf, 64, TEST_LEN(m))) {
			printf("alloc error: Fail");
			return -1;
		}
		temp_buffs[i] = buf;
	}

	// Test erasure code by encode and recovery

	// Pick a first test
	if (m > MMAX || k > KMAX) {
                printf(" m (%d) > MMAX (%d), or k (%d) > KMAX (%d)\n",
                     m, MMAX, k, KMAX);
		return -1;
        }


	// Make random data
	for(i=0; i<k; i++)
		for(j=0; j<(TEST_LEN(m)); j++)
			buffs[i][j] = rand();


	memset(src_in_err, 0, TEST_SOURCES);

	srand(TEST_LEN(m));
	for (i=0, nerrs=0; i<k && nerrs<m-k; i++){
		err = (rand()%3 == 0);
		src_in_err[i] = err;
		if (err)
			src_err_list[nerrs++] = i;
	}
	if (nerrs == 0){  // should have at least one error
		while ((err = (rand() % KMAX)) >= k) ;
		src_err_list[nerrs++] = err;
		src_in_err[err] = 1;
	}
	printf("Test erase list = ");
	for (i=0; i<nerrs; i++)
		printf(" %d", src_err_list[i]);
	printf("\n");
        printf("Print out Error list: nerrs %d\n", nerrs);
        dump(src_err_list, nerrs);
        printf("Print out Original data for Error list\n");
        for (i=0; i<nerrs; i++) {
            dump(buffs[src_err_list[i]], 25);
        }

        loop_multiplier = TEST_LEN(m)/stripe_size;
	perf_start(&start);

	for (rtest = 0; rtest < (loop_multiplier * TEST_LOOPS(m)); rtest++){
		gf_gen_rs_matrix(a, m, k);

		// Make parity vects
		ec_init_tables(k, m-k, &a[k*k], g_tbls);
		ec_encode_data_sse(
                   stripe_size,   // len = length of each block of data (vector) of source data
		   k,             // k = number of vector sources or rows in the generator matrix for coding 
                   m-k,           // rows = number of output vectors to concurrently encode/decode
                   g_tbls,        // gftbls = pointer to array of input tables. generated from coding coeff.
                   buffs,         // input data buffer
                   &buffs[k]);     // output data buffer
	}

	perf_stop(&stop);
        printf("stripe_size=%d, m=%d rtest=%d\n", stripe_size, m, rtest);
	printf("erasure_code_sse_encode" TEST_TYPE_STR ": ");
	perf_print(stop,start,
			(long long)stripe_size*(m)*rtest);

	perf_start(&start);

	for (rtest = 0; rtest < loop_multiplier*TEST_LOOPS(m); rtest++){
		// Construct b by removing error rows
		for(i=0, r=0; i<k; i++, r++){
			while (src_in_err[r]) 
				r++;
			for(j=0; j<k; j++)
				b[k*i+j] = a[k*r+j];
		}
                if (!printed_errors) {
                    printed_errors = 1;
                    printf("Print out Error data \n");
                    for (i=0; i<nerrs; i++) {
                        dump(&b[src_err_list[i]], 25);
                    }
                }

		if (gf_invert_matrix(b, d, k) < 0){
			printf("BAD MATRIX\n");
			return -1;
		}

		for(i=0, r=0; i<k; i++, r++){
			while (src_in_err[r]) 
				r++;
			recov[i] = buffs[r];
		}

		for(i=0; i<nerrs; i++){
			for(j=0; j<k; j++){
				c[k*i+j]=d[k*src_err_list[i]+j];
			}
		}

		// Recover data
		ec_init_tables(k, nerrs, c, g_tbls);
		ec_encode_data_sse(stripe_size,
				k, nerrs, g_tbls, recov, &temp_buffs[k]);

	}
        printf("Print out Recovery data after Decoding\n");
        for (i=0; i<nerrs; i++) {
            dump(temp_buffs[k+i], 25);
        }
	
	perf_stop(&stop);
	for(i=0; i<nerrs; i++){
		if (0 != memcmp(temp_buffs[k+i], buffs[src_err_list[i]],
				stripe_size)){
			printf("Fail error recovery (%d, %d, %d) - ", 
				m, k, nerrs);
			printf(" - erase list = ");
			for (j=0; j<nerrs; j++)
				printf(" %d", src_err_list[j]);
			printf("\na:\n"); 
			dump_u8xu8((u8*)a, m, k);
			printf("inv b:\n");
			dump_u8xu8((u8*)d, k, k);
			printf("orig data:\n"); 
			dump_matrix(buffs, m, 25);
			printf("orig   :");
			dump(buffs[src_err_list[i]],25);
			printf("recov %d:",src_err_list[i]);
			dump(temp_buffs[k+i], 25);
			return -1;
		}
	}

	printf("erasure_code_sse_decode" TEST_TYPE_STR ": ");
	perf_print(stop,start,
			((long long)stripe_size*(k+nerrs)*rtest));

	printf("done all: Pass\n");
	return 0;
}

